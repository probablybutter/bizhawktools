This is a group of useful Bizhawk Lua scripts and memory address watch files for Majora's Mask.

It's designed to work for the NTSC (EN), JP 1.0, and JP 1.1 roms. In some cases, some functions may be limited to just one of these ROM's.

# Folder organization

- `lua` contains all the lua scripts. Scripts prefixed with `bin_` are top level "programs", while scripts prefixed with `lib_` are support libraries. Yes, this organization is dumb, but trying to setup Lua to handle the paths correctly is really convoluted and dumb.

