local camera_info = {}

local core = dofile('lib_core.lua')

local version = core.version

if(version == 0) then
   -- english
   camera_info.camera_addr = 0x3e6c00
elseif(version == 1) then
   -- JP 1.0
   print('JP 1.0 not supported yet')
   return
elseif(version == 2) then
   -- JP 1.1
   print('JP 1.1 not supported yet')
   return
else
   print('Unknown ROM')
   return
end

function camera_info.source_pos()
   --- Gets the location of the camera
   --- Note: there are a few situations where this doesn't seem to be correct (I think when targetting?)
   return memory.readfloat(camera_info.camera_addr, true, 'RDRAM'),
   memory.readfloat(camera_info.camera_addr+4, true, 'RDRAM'),
   memory.readfloat(camera_info.camera_addr+8, true, 'RDRAM')
end

function camera_info.target_pos()
   --- Gets the location of where the camera is focusing on
   --- Note: there are a few situations where this doesn't seem to be correct (I think when targetting?)
   return memory.readfloat(camera_info.camera_addr+12, true, 'RDRAM'),
   memory.readfloat(camera_info.camera_addr+16, true, 'RDRAM'),
   memory.readfloat(camera_info.camera_addr+20, true, 'RDRAM')
end

return camera_info
