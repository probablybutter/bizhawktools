local actor_info = {}

local core = dofile('lib_core.lua')

local version = core.version

if(version == 0) then
   -- english
   actor_info.actors_table = 0x3E87D0
elseif(version == 1) then
   -- JP 1.0
   actor_info.actors_table = 0x3e89a4
elseif(version == 2) then
   -- JP 1.1
   actor_info.actors_table = 0x3e8c60
else
   print('Unknown ROM')
   return
end

function actor_info.read(actor, name)
   ---
   --- Helper for reading something from an actor
   --- @param actor TODO: describe
   --- @param name TODO: describe
   ---
   local key = actor[name]
   local addr = actor.addrs[key]
   return actor.read_funs[key](addr, 'RDRAM')
end

function actor_info.add_key(actor, name, addr, fmt, read_fun, write_fun)
   ---
   --- Associates a given address with an actor as a certain type of paramter.
   ---
   --- @param actor TODO: describe
   --- @param name Name of the parameter
   --- @param addr Address in memory of the parameter
   --- @param fmt printf format used for printing out the parameter
   --- @param read_fun function to call for reading the parameter
   --- @param write_fun function to call for writing the parameter
   ---
   table.insert(actor.addrs, addr)
   table.insert(actor.keys, name)
   table.insert(actor.fmt, fmt)
   table.insert(actor.read_funs, read_fun)
   table.insert(actor.write_funs, write_fun)
   actor[actor.keys[#actor.keys]] = #actor.addrs
end

read_float_be = core.read_float_be
write_float_be = core.write_float_be

function actor_info.num_actors(actor_type)
   --- Determines how many actors are loaded with the given actor type
   --- @param actor_type integer in range [0,12)
   return memory.read_u32_be(actor_info.actors_table+3*actor_type*4, 'RDRAM')
end

function actor_info.find_actor_addr(actor_type, idx)
   ---
   --- Each actor type is stored in a list.
   --- This tries to find the actor at index idx of a given type.
   --- does not include the 0x80 MSB
   
   local curr_actor = memory.read_u32_be(actor_info.actors_table+3*actor_type*4+4,'RDRAM')
   for i = 1,idx do
      if(curr_actor == 0) then
         return 0
      end
      curr_actor = memory.read_u32_be(curr_actor+300-0x80000000,'RDRAM')
   end
   if(curr_actor == 0) then
      return 0
   end
   return curr_actor - 0x80000000
end

function actor_info.add_unknown(actor_addr)
   --- Adds the unknown fields of an actor so it can be read using the actor_info.read function.
   --- TODO: need to figure out the actual size of the given actor
   --- @param actor_addr The start address of the actor (use actor_info.find_actor_addr)

   local res = {}
   res.addrs = {}
   res.keys = {}
   res.fmt = {}
   res.read_funs = {}
   res.write_funs = {}
   local addr = actor_addr+4
   -- after room number
   addr = addr + 4+4*3+2+2+4+4+4+4*3+2*4
   -- after rotation z 1
   -- always 0?
   --actor_info.add_key(res, 'unknown 02', addr, '0x%08x', read_float_be, write_float_be)
   addr = addr + 4+4*3+2*4
   -- after rotation z 2
   -- always 0?
   --actor_info.add_key(res, 'unknown 02', addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
   addr = addr + 4
   actor_info.add_key(res, 'unknown 03', addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
   addr = addr + 4+4*3
   -- after scale z
   local fmt = 'buf 0 %02x'
   addr = addr + 4*4
   for i =7,0xb do
      actor_info.add_key(res, fmt:format(i), addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
      addr = addr + 4
   end
   addr = addr + 4*3
   -- 28 bytes of stuff?
   local fmt = 'buf 1 %02x'
   for i=0,6 do
      actor_info.add_key(res, fmt:format(i), addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
      addr = addr + 4
   end
   --addr = addr + 28

   addr = addr + 2*4

   -- after rotation z 3
   fmt = 'buf 2 %02x'
   for i=0,0x03 do
      actor_info.add_key(res, fmt:format(i), addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
      addr = addr + 4
   end
   for i=0x04,0x09 do
      addr = addr + 4
      -- always 0?
      --actor_info.add_key(res, fmt:format(i), addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
   end
   for i=0x0a,0x0d do
      -- something related to camera?
      --actor_info.add_key(res, fmt:format(i), addr, '%f', read_float_be, write_float_be)
      addr = addr + 4
   end
   for i=0x0e,0x10 do
      actor_info.add_key(res, fmt:format(i), addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
      --actor_info.add_key(res, fmt:format(i), addr, '%f', read_float_be, write_float_be)
      addr = addr + 4
   end
   addr = addr + 12

   for i=0x14,0x15 do
      actor_info.add_key(res, fmt:format(i), addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
      addr = addr + 4
   end
   addr = addr + 4
   for i=17,24 do
      actor_info.add_key(res, fmt:format(i), addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
      addr = addr + 4
   end

   -- after actor_next
   fmt = 'unknown %02x'
   addr = addr + 8
   -- unknown 04 seems to always be 0?
   for i=1,14 do
      actor_info.add_key(res, fmt:format(4+i), addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
      addr = addr + 4
   end
   actor_info.add_key(res, "action function?", addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
   addr = addr + 4+4*4+20*4
   for i=1,50 do
      actor_info.add_key(res, fmt:format(19+i), addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
      addr = addr + 4
   end
   
   return res
end

function actor_info.add_actor_base(actor_addr)
   --- Adds the "known" fields of an actor so it can be read using the actor_info.read function.
   --- @param actor_addr The start address of the actor (use actor_info.find_actor_addr)
   local res = {}
   res.addrs = {}
   res.keys = {}
   res.fmt = {}
   res.read_funs = {}
   res.write_funs = {}
   local addr = actor_addr

   actor_info.add_key(res, 'actor number', addr, '0x%04x', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 2

   actor_info.add_key(res, 'actor type', addr, '0x%02x', memory.readbyte, memory.writebyte)
   addr = addr + 1

   actor_info.add_key(res, 'room number', addr, '0x%02x\n', memory.readbyte, memory.writebyte)
   addr = addr + 1
   actor_info.add_key(res, 'actor flags', addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
   addr = addr + 4

   actor_info.add_key(res, 'initial x', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'initial y', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'initial z', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4

   actor_info.add_key(res, 'initial rotation x', addr, '0x%04x', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 2
   actor_info.add_key(res, 'initial rotation y', addr, '0x%04x', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 2
   actor_info.add_key(res, 'initial rotation z', addr, '0x%04x\n', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 4

   actor_info.add_key(res, 'actor variable', addr, '0x%04x', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 4

   actor_info.add_key(res, 'play sound effect', addr, '0x%04x\n', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 4

   actor_info.add_key(res, 'position x 1', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'position y 1', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'position z 1', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4

   actor_info.add_key(res, 'rotation x 1', addr, '0x%04x', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 2
   actor_info.add_key(res, 'rotation y 1', addr, '0x%04x', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 2
   actor_info.add_key(res, 'rotation z 1', addr, '0x%04x\n', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 8

   actor_info.add_key(res, 'position x 2', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'position y 2', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'position z 2', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4

   actor_info.add_key(res, 'rotation x 2', addr, '0x%04x', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 2
   actor_info.add_key(res, 'rotation y 2', addr, '0x%04x', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 2
   actor_info.add_key(res, 'rotation z 2', addr, '0x%04x\n', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 12

   actor_info.add_key(res, 'scale x', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'scale y', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'scale z', addr, '%f\n', read_float_be, write_float_be)
   addr = addr + 4

   -- Note: this is game's reported velocity
   -- game only ever writes to this, so modifying the value with ram watch doesn't effect the game?
   -- is not accurate if actor is attached to something else (ex.: link carrying a pot)
   -- any other reasons?
   -- best measure of true velocity is probably: curr position - prev position
   actor_info.add_key(res, 'velocity 1 x', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'velocity 1 y', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'velocity 1 z', addr, '%f\n', read_float_be, write_float_be)
   addr = addr + 4
   -- read only?
   actor_info.add_key(res, 'horizontal speed', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   addr = addr + 32
   
   actor_info.add_key(res, 'distance^2 to player', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'horizontal distance to player', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'vertical distance to player', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4

   addr = addr + 20
   -- might have other uses for actors which aren't enemies?
   actor_info.add_key(res, 'actor health', addr+3, '%d', memory.readbyte, memory.writebyte)
   addr = addr + 4

   addr = addr + 4

   actor_info.add_key(res, 'rotation x 3', addr, '0x%04x', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 2
   actor_info.add_key(res, 'rotation y 3', addr, '0x%04x', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 2
   actor_info.add_key(res, 'rotation z 3', addr, '0x%04x\n', memory.read_u16_be, memory.write_u16_be)
   addr = addr + 72

   actor_info.add_key(res, 'more flags/counters', actor_addr+284, '0x%08x', memory.read_u32_be, memory.write_u32_be)

   actor_info.add_key(res, 'prev position x 1', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'prev position y 1', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4
   actor_info.add_key(res, 'prev position z 1', addr, '%f', read_float_be, write_float_be)
   addr = addr + 4

   addr = addr + 20

   actor_info.add_key(res, 'previous actor', addr, '0x%08x', memory.read_u32_be, memory.write_u32_be)
   addr = addr + 4
   actor_info.add_key(res, 'next actor', addr, '0x%08x\n', memory.read_u32_be, memory.write_u32_be)

   -- TODO: actor-specific information?
   return res
end

return actor_info
