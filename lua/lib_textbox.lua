textbox = {}

-- http://wiki.cloudmodding.com/mm/Text_Format
local en_version_data = {}
en_version_data.tb_status_addr = 0x3fd34a
-- identifies what textbox
en_version_data.tb_number = 0x3fd32c
en_version_data.slowtext_addr = 0x3fd41b
en_version_data.tb_data_addr = 0x3fce10+11
en_version_data.box_breaks = {0x10, 0x12}
en_version_data.read_char_fun = memory.readbyte
en_version_data.char_size = 1
en_version_data.end_marker = 0xBF
en_version_data.ignore_buttons = 0x1A
en_version_data.pause_1 = 0x1B
en_version_data.pause_2 = 0x1C
en_version_data.pause_3 = 0x1D
en_version_data.sound_effect = 0x1E
en_version_data.pause_4 = 0x1F
en_version_data.choice2 = 0xC2
en_version_data.choice3 = 0xC3
en_version_data.item_prompt = 0xD2
en_version_data.delayed_skippable = 0x19
en_version_data.menu_vcursor_loc = 0x3fd449
-- what item is being selected for purchase in the bomb shop
en_version_data.bomb_shop_selected = 0x40fe22
-- what item is being selected for purchase in the clocktown trading post
-- diagonal inputs allowed, but no wrapping along sides
en_version_data.clocktown_shop_selected = 0x4116E6

-- bomb shop can advance past first textbox when *(0x8040fc88) = 0x8040C29C
-- bomb shop owner can move when *(0x8040fc88) = 0x8040c860

local jp1_0_version_data = {}
jp1_0_version_data.tb_status_addr = 0x3FD51A
jp1_0_version_data.slowtext_addr = 0x3fd5eb
jp1_0_version_data.tb_data_addr = 0x3FCFE0+12
jp1_0_version_data.box_breaks = {0x0009, 0x000B}
jp1_0_version_data.read_char_fun = memory.read_u16_be
jp1_0_version_data.char_size = 2
jp1_0_version_data.end_marker = 0x0500
jp1_0_version_data.ignore_buttons = 0x0104
jp1_0_version_data.pause_1 = 0x0110
jp1_0_version_data.pause_2 = 0x0111
jp1_0_version_data.pause_3 = 0x0112
jp1_0_version_data.sound_effect = 0x0120
jp1_0_version_data.pause_4 = 0x0128
jp1_0_version_data.choice2 = 0x0202
jp1_0_version_data.choice3 = 0x0203
jp1_0_version_data.item_prompt = 0x0222
jp1_0_version_data.delayed_skippable = 0x0103

local jp1_1_version_data = {}
jp1_1_version_data.tb_status_addr = 0x3fd7da
jp1_1_version_data.slowtext_addr = 0x3fd8ab
jp1_1_version_data.tb_data_addr = 0x3fd2a0+12
jp1_1_version_data.box_breaks = {0x0009, 0x000B}
jp1_1_version_data.read_char_fun = memory.read_u16_be
jp1_1_version_data.char_size = 2
jp1_1_version_data.end_marker = 0x0500
jp1_1_version_data.ignore_buttons = 0x0104
jp1_1_version_data.pause_1 = 0x0110
jp1_1_version_data.pause_2 = 0x0111
jp1_1_version_data.pause_3 = 0x0112
jp1_1_version_data.sound_effect = 0x0120
jp1_1_version_data.pause_4 = 0x0128
jp1_1_version_data.choice2 = 0x0202
jp1_1_version_data.choice3 = 0x0203
jp1_1_version_data.item_prompt = 0x0222
jp1_1_version_data.delayed_skippable = 0x0103
jp1_1_version_data.bomb_shop_selected = 0x410482
jp1_1_version_data.menu_vcursor_loc = 0x3fd8d9
jp1_1_version_data.clocktown_shop_selected = 0x412056
jp1_1_version_data.potion_shop_selected = 0x41076c
jp1_1_version_data.goron_shop_selected = 0x410282
jp1_1_version_data.zora_shop_selected = 0x410482

-- check if ROM is EN, JP 1.0, or JP 1.1
local core = dofile('lib_core.lua')
local version = core.version
if(version == 1) then
   -- japanese 1.0 ROM
   textbox.version_data = jp1_0_version_data
elseif(version == 2) then
   -- japanese 1.1 ROM
   textbox.version_data = jp1_1_version_data
elseif(version == 0) then
   -- US
   textbox.version_data = en_version_data
else
   -- unknown version
   print('Unknown ROM language/version')
   return textbox
end

function advance_with_callback(cb, ctx)
   emu.frameadvance()
   if(cb ~= nil) then
      cb(ctx)
   end
end

-- shop items:
-- text status goes to 5 during transition to item, 6 on starting to display item description
-- accepting inputs when text status goes to 42
-- pressing A takes priority over direction input

-- menus:
-- can send another A input one visual frame after moving, but need one visual frame gap
-- between movement inputs (all cases)


function textbox.handle_textbox(frame_callback, menu_callback, frame_ctx, menu_ctx)
   --- advances textboxes without options, minimal amount of inputs
   --- takes advantage of quicktext
   --- @param frame_callback Optional function to call everytime this function advances a frame. Has signature: fun(frame_ctx)
   --- @param menu_callback Optional function to call when a menu textbox has been detected. Has signature: fun(frame_callback, frame_ctx, menu_ctx)
   --- @param frame_ctx Context used for the frame callback
   --- @param menu_ctx Context used for the menu callback
   ---
   --- @return Pair of integers for (start, stop) frames of the textbox. Note: there might be certain scenarios when this doesn't quite work right. Still need to fix.
   
   -- text status flag (byte, 0x803fd34a):
   -- 0x0 = no textbox
   -- 0x1 = start?
   -- 0x2 = open?
   -- 0x3 = open process?
   -- 0x4 = open process?
   -- 0x5 = open process?
   -- 0x6 = drawing characters (quicktext you can press B)
   -- 0x8 = intermediate pause?
   -- 0x9 = intermediate pause
   -- 0x41 = accepting input to next textbox
   -- 0x42 = accepting input to close textbox
   -- 0x43 = text box closing
   -- 0x44 = ?
   -- 0x45 = dawn of day text/area name text open?
   -- 0x46 = area name text typing
   -- 0x47 = area name text done
   -- 0x48 = area name text closing
   -- 0x49 = area name text closed

   -- optimal strategy (thanks to Eumeus):
   --   If textbox is quicktext with more than 1 page of text:
   --      press B as early as possible for 1 frame, then A as soon as possible to close box
   --   If textbox is quicktext with exactly 1 page of text:
   --      Press A for 3 frames, release for 1 input frame, then press A for 2 input frames
   --   If textbox is slowtext:
   --      Press A for 1 frame as soon as possible

   -- TODO:
   --   fix 0x19/0x0103 (unskippable text, used by bomb salesman, after postman minigame)
   --   some text boxes aren't detecting the start of a textbox correctly?
   --      postboxes
   --      anju asking for room key (jp?)
   --      others?
   --

   local version_data = textbox.version_data

   local tb_status_addr = version_data.tb_status_addr
   local slowtext_addr = version_data.slowtext_addr
   local tb_data_addr = version_data.tb_data_addr
   local box_breaks = version_data.box_breaks
   local read_char_fun = version_data.read_char_fun
   local char_size = version_data.char_size
   local end_marker = version_data.end_marker
   local ignore_buttons = version_data.ignore_buttons
   -- waits for a time yyxx, then prints rest of text (new page?).
   -- User can advance text up to pause char (fast text only)
   local pause_1 = version_data.pause_1
   -- waits for a time yyxx, then closes textbox automatically.
   -- Serves as an end token
   -- User can't advance text manually
   local pause_2 = version_data.pause_2
   -- waits for a time yyxx, then closes textbox automatically.
   -- Serves as an end token
   -- User can advance text manually
   local pause_3 = version_data.pause_3
   -- sound effect: char xx xx
   local sound_effect = version_data.sound_effect
   -- pauses text for xxyy time (no new page/line)
   -- quicktext can advance past these points
   local pause_4 = version_data.pause_4
   -- two-choice selection
   local choice2 = version_data.choice2
   -- three-choice selection
   local choice3 = version_data.choice3
   -- prompt for item
   local item_prompt = version_data.item_prompt

   -- TODO: other textboxes like bomber's code/postman minigame?

   local start_frame = nil

   while(true) do
      local text_status = memory.readbyte(tb_status_addr,'RDRAM')
      if(text_status ~= 0 and start_frame == nil) then
         start_frame = emu.framecount()
      end
      if(text_status == 0 and start_frame ~= nil) then
         return start_frame, emu.framecount()
      end
      if(text_status == 6) then
         -- parse the message data
         local found_end = false
         local i = 0
         local num_pages = 1
         local is_slowtext = memory.readbyte(slowtext_addr,'RDRAM')
         while(found_end == false) do
            local val = read_char_fun(tb_data_addr+i,'RDRAM')
            if(val == end_marker) then
               found_end = true
            elseif(val == ignore_buttons) then
               -- ignore this type of textbox
               print('ignore buttons textbox, ignoring')
               return start_frame, emu.framecount()
            elseif(val == pause_1) then
               if(is_slowtext == 0) then
                  -- can advance up to the pause point
                  print('pause slowtext B')
                  joypad.set({['B']=true}, 1)
                  advance_with_callback(frame_callback, frame_ctx)
                  joypad.set({['B']=false}, 1)
                  -- TODO: how to wait for end of timer?
                  client.pause()
               end
               -- skip timer
               i = i + 2
            elseif(val == pause_2) then
               -- no user input for the last page
               num_pages = num_pages - 1
               found_end = true
               -- skip timer
               i = i + 2
            elseif(val == pause_3) then
               -- user can advance text manually, treat as normal end token
               found_end = true
               -- skip timer
               i = i + 2
            elseif(val == sound_effect) then
               -- skip sound effect code
               i = i + 2
            elseif(val == pause_4) then
               -- skip timer
               i = i + 2
            elseif(val == choice2) then
               -- TODO: handle 2 options
               if(menu_callback ~= nil) then
                  menu_callback(frame_callback, frame_ctx, menu_ctx)
               end
            elseif(val == choice3) then
               -- TODO: handle 3 options
               if(menu_callback ~= nil) then
                  menu_callback(frame_callback, frame_ctx, menu_ctx)
               end
            elseif(val == item_prompt) then
               -- ignore item prompt textboxes for now
               print('item prompt')
               print('skipping...')
               return start_frame, emu.framecount()
            else
               -- is character a box break?
               for j,v in ipairs(box_breaks) do
                  if(v == val) then
                     num_pages = num_pages + 1
                     break
                  end
               end
            end
            i = i + char_size
         end

         if(is_slowtext == 1) then
            if(num_pages >= 1) then
               -- slow text
               while(true) do
                  text_status = memory.readbyte(tb_status_addr,'RDRAM')
                  if(text_status == 0x41 or text_status == 0x42) then
                     -- can press A
                     joypad.set({['A']=true}, 1)
                     advance_with_callback(frame_callback, frame_ctx)
                     joypad.set({['A']=false}, 1)
                     advance_with_callback(frame_callback, frame_ctx)
                     if(text_status == 0x42) then
                        return start_frame, emu.framecount()
                     end
                  end
                  advance_with_callback(frame_callback, frame_ctx)
               end
            else
               advance_with_callback(frame_callback, frame_ctx)
            end
         else
            if(num_pages == 1) then
               -- Press A for 3 frames, release for 1 input frame, then press A for 2 input frames
               for i=1,3 do
                  joypad.set({['A']=true}, 1)
                  advance_with_callback(frame_callback, frame_ctx)
               end
               joypad.set({['A']=false}, 1)
               advance_with_callback(frame_callback, frame_ctx)
               joypad.set({['A']=true}, 1)
               advance_with_callback(frame_callback, frame_ctx)
               joypad.set({['A']=true}, 1)
               advance_with_callback(frame_callback, frame_ctx)
               joypad.set({['A']=false}, 1)
            elseif(num_pages > 1) then
               -- press B
               joypad.set({['B']=true}, 1)
               advance_with_callback(frame_callback, frame_ctx)
               joypad.set({['B']=false}, 1)
               -- search for text_status 0x41/0x42
               while(true) do
                  text_status = memory.readbyte(tb_status_addr,'RDRAM')
                  if(text_status == 0x41) then
                     print("wasn't expecting 0x41 in quicktext")
                     break
                  end
                  if(text_status == 0x42) then
                     break
                  end
                  advance_with_callback(frame_callback, frame_ctx)
               end
               -- close/advance
               joypad.set({['A']=true}, 1)
               emu.frameadvance()
               joypad.set({['A']=false}, 1)
            end
         end
         return start_frame, emu.framecount()
      end
      advance_with_callback(frame_callback, frame_ctx)
   end
end

return textbox
