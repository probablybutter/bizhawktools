---
--- This is a basic auto advance textbox script.
--- As far as I know this uses optimal TAS strats, though there are a few notable cases
--- which the script doesn't handle correctly. These are:
---   - Textboxes with menus
---   - Shops
---   - Textboxes where you're required to press a C button (like depositing a letter in the mailbox)
---
--- There might be a few other cases which don't work correctly? Hopefully it's obvious
--- in the cases when the script doesn't work optimally, and you can manually TAS these sections.

local textbox = dofile('lib_textbox.lua')

while(true) do
   textbox.handle_textbox()
end
