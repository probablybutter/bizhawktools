--[[
   Prints some actor information to the screen. Allows you to navigate between different actors.
--]]

function printf(s,...)
   gui.text(x, y, s:format(...))
   y = y + 14
   if(y > 700) then
      y = 40
      x = 500
   end
end

function print_actor(actor)
   printf('actor idx: %d/%d', current_actor, num_actors)
   for i,addr in ipairs(actor.addrs) do
      printf('0x%08x %s: %s', addr, actor.keys[i], actor.fmt[i]:format(actor.read_funs[i](addr,'RDRAM')))
   end
end

actor_type = 0
current_actor = 0
num_actors = 0

actor_info = dofile('lib_actor_info.lua')
read_type = actor_info.add_actor_base

function do_update()
   gui.cleartext()
   x = 0
   y = 40
   -- limit actor_type
   if(actor_type < 0) then
      actor_type = 0
   end
   if(actor_type > 11) then
      actor_type = 11
   end
   num_actors = actor_info.num_actors(actor_type)
   -- limit current_actor
   if(current_actor >= num_actors) then
      current_actor = num_actors - 1
   end
   if(num_actors > 0 and current_actor < 0) then
      current_actor = 0
   end
   
   -- find the current actor
   local actor_addr = actor_info.find_actor_addr(actor_type, current_actor)

   -- display actor data
   if(actor_addr > 0) then
      local actor_data = read_type(actor_addr)
      print_actor(actor_data)
   end
end

function prev_actor_fun()
   current_actor = current_actor - 1
   do_update()
end

function next_actor_fun()
   current_actor = current_actor + 1
   do_update()
end

function prev_actor_type_fun()
   actor_type = actor_type - 1
   do_update()
end

function next_actor_type_fun()
   actor_type = actor_type + 1
   do_update()
end

function set_read_known()
   read_type = actor_info.add_actor_base
   do_update()
end

function set_read_unknown()
   read_type = actor_info.add_unknown
   do_update()
end

running = true
function do_close()
   running = false
end

form_id = forms.newform(320, 240, 'actor control', do_close)
prev_actor = forms.button(form_id, 'prev actor', prev_actor_fun, 0, 0)
next_actor = forms.button(form_id, 'next actor', next_actor_fun, 0, 40)
prev_actor_type = forms.button(form_id, 'prev type', prev_actor_type_fun, 200, 0)
next_actor_type = forms.button(form_id, 'next type', next_actor_type_fun, 200, 40)

read_known_btn = forms.button(form_id, 'known', set_read_known, 0, 80)
read_unknown_btn = forms.button(form_id, 'unknown', set_read_unknown, 200, 80)

while running == true do
   do_update()
   
   emu.yield()
end
