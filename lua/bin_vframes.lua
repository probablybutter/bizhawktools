--[[
   Repeatedly advance visual frames
   User should enable this script and then use play to advance a visual frame
--]]

local itools = dofile('lib_input_tools.lua')

while(true) do
   itools.vframe_advance(nil, nil, true)
   print(emu.framecount())
end
